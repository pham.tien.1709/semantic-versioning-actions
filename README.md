# Semantic versioning without any track files

**This action uses git tag for controlling the version. This action will not change/add/remove any file in your repository.**

## How to use

### Use script: run this script in your repository

- Create a sh file with below content `run_sample.sh`.

```sh
source /dev/stdin <<<"$(curl -s https://gitlab.com/pham.tien.1709/semantic-versioning-actions/-/raw/main/index.sh)" && get_stage_prompt

```

- Run that file with bash (on Windows with [git bash](https://gitforwindows.org/))

```sh
bash run_sample.sh
```

- Script will show the menu options:

```bash
Action:
Action:
1) Increment development environment  5) Increment patch version (1.0.xx)
2) Increment staging environment      6) Increment minor version (1.xx.0)
3) Increment UAT environment          7) Increment major version (xx.0.0)
4) Increment product environment      8) Quit
Choose: 1
Chose option: Increment development environment
Enumerating objects: 1, done.
Counting objects: 100% (1/1), done.
Writing objects: 100% (1/1), 171 bytes | 171.00 KiB/s, done.
Total 1 (delta 0), reused 0 (delta 0), pack-reused 0
To github.com:tuanngocptn/semantic-versioning-action.git
 * [new tag]         v5.0.4-dev+1 -> v5.0.4-dev+1
```

### Thanks for using Semantic Versioning Action
